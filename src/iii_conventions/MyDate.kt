package iii_conventions

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override fun compareTo(other: MyDate): Int {
        return compareValuesBy(this, other, MyDate::year, MyDate::month, MyDate::dayOfMonth)
    }

    operator fun plus(interval: TimeInterval) = addTimeIntervals(interval, 1)

    operator fun plus(timeIntervalTimes: TimeIntervalTimes) = addTimeIntervals(timeIntervalTimes.timeInterval, timeIntervalTimes.times)
}

operator fun MyDate.rangeTo(other: MyDate): DateRange = DateRange(this, other)

enum class TimeInterval {
    DAY,
    WEEK,
    YEAR;

    operator fun times(times: Int) = TimeIntervalTimes(this, times)
}

data class TimeIntervalTimes(val timeInterval: TimeInterval, val times: Int)

class DateRange(val start: MyDate, val endInclusive: MyDate) : Iterable<MyDate> {
    operator fun contains(other: MyDate) = other >= start && other <= endInclusive

    override fun iterator(): Iterator<MyDate> = object : Iterator<MyDate> {

        var current = start;

        override fun hasNext() = current <= endInclusive;

        override fun next() = current.apply { current = current.nextDay() }

    }
}
